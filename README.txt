
s3394328    -   Sander Beyen

This repository contains all the content of my final project submission for the course Introduction to research methods:

- Jupyter Notebook with data analysis
- Dataset in .csv format
- Final version of the project report in .pdf format

